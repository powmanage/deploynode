echo "fs.file-max = 65535" >> /etc/sysctl.conf
sysctl -p
echo "*               hard    nofile          65535" >> /etc/security/limits.conf
echo "*               soft    nofile          65535" >> /etc/security/limits.conf
echo "root            hard    nofile          65535" >> /etc/security/limits.conf
echo "root            soft    nofile          65535" >> /etc/security/limits.conf
echo "DefaultLimitCORE=infinity" >> /etc/systemd/system.conf
echo "DefaultLimitNOFILE=65535" >> /etc/systemd/system.conf
echo "DefaultLimitNPROC=65535" >> /etc/systemd/system.conf
systemctl daemon-reexec
echo "need run 'reboot' cmd now, after reboot, use 'ulimit -n' to check, right output is 65535."
